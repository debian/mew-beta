Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Mew
Source: https://github.com/kazu-yamamoto/Mew

Files: *
Copyright: 1994-2023, Mew developing team
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the team nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: etc/mew-*.xpm
Copyright: 1997, Yoshiaki Kasahara <kasahara@nc.kyushu-u.ac.jp>
           1997-2003, Yuuichi Teranishi <teranisi@gohome.org>
           1997-2003, Kazu Yamamoto <kazu@Mew.org>
           2004, Yoshifumi Nishida <nishida@csl.sony.co.jp>
License: BSD-3-clause
Comment:
 Copyright notice is the same as Mew's one.

Files: install-sh
Copyright: 1991, the Massachusetts Institute of Technology
License: permissive-install-sh
 Permission to use, copy, modify, distribute, and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that
 the above copyright notice appear in all copies and that both that
 copyright notice and this permission notice appear in supporting
 documentation, and that the name of M.I.T. not be used in advertising or
 publicity pertaining to distribution of the software without specific,
 written prior permission.  M.I.T. makes no representations about the
 suitability of this software for any purpose.  It is provided "as is"
 without express or implied warranty.

Files: bin/configure configure
Copyright: 1992-2021, Free Software Foundation
License: permissive-configure
 This configure script is free software; the Free Software Foundation
 gives unlimited permission to copy, distribute and modify it.

Files: bin/cmew.1 bin/mew-pinentry.1 bin/mewest.1 bin/smew.1
Copyright: 2006-2008, Mew developing team
License: permissive-mew-man
 Unlimited permission is granted to use, copy, distribute and/or modify
 this file.  There is NO WARRANTY.
Comment:
 Tatsuya Kinoshita contributed to Mew developing team.

Files: CHANGES/gitlog2mewchanges
Copyright: 2013, Tatsuya Kinoshita
License: permissive-gitlog2mewchanges
 Redistribution and use in source and binary forms, with or without
 modification, are permitted without restriction, with NO WARRANTY.
 You may regard this as a work that is placed in the public domain.

Files: debian/*
Copyright: 2003-2023, Tatsuya Kinoshita <tats@debian.org>
           2001-2003, NOSHIRO Shigeo <noshiro@debian.org>
           1999-2001, ISHIKAWA Mutsumi <ishikawa@linux.or.jp>
           1996-1998, Yoshiaki Yanagihara <yochi@debian.or.jp>
License: permissive-debian
 There is no restriction, so this package may be distributed under
 the same conditions as the upstream.

Files: debian/dot.*
Copyright: 2021, Tatsuya Kinoshita
License: CC0-1.0
 Released under the terms of the CC0 Public Domain Dedication.
 <https://creativecommons.org/publicdomain/zero/1.0/>
Comment:
 On Debian systems, the complete text of the Creative Commons CC0 1.0
 Universal Public Domain Dedication can be found in
 "/usr/share/common-licenses/CC0-1.0".

Files: debian/mewstunnel*
Copyright: 2005-2014, Tatsuya Kinoshita
License: permissive-mewstunnel
 Redistribution and use in source and binary forms, with or without
 modification, are permitted without restriction, with NO WARRANTY.
 You may regard this as a work that is placed in the public domain.

Files: info/*
Copyright: 1996-2023, Mew developing team
License: permissive-info

License: permissive-info
 The copyright of this manual belongs to the author. Permission is
 granted to copy, modify, redistribute this manual but no warranty.

Files: debian/patches/*
Copyright: 1994-2023, Mew developing team
License: BSD-3-clause and permissive-info

Files: debian/patches/010_contrib.patch
Copyright: 1994-2007, Mew developing team
           1998-2007, Hideyuki SHIRAI <shirai@mew.org>
           2001, Shun-ichi TAHARA <jado@flowernet.gr.jp>
License: BSD-3-clause
Comment:
 mew-absfilter.el is provided by SAITO Takuya under the permissive terms:
 You can use, copy, distribute, and/or modify this file for any purpose.
 There is NO WARRANTY.
